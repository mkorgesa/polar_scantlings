'''
Created by M.Kõrgesaar August 2020

I2.5 Framing - General 

I2.5.1 Framing members of Polar Class ships are to be designed to withstand the ice loads 
defined in I2.3. 

I2.5.2 The term “framing member” refers to transverse and longitudinal local frames, load-
carrying stringers and web frames in the areas of the hull exposed to ice pressure, see Figure 
1. Where load-distributing stringers have been fitted, the arrangement and scantlings of these 
are to be in accordance with the requirements of the Classification Society. 

I2.5.3 The strength of a framing member is dependent upon the fixity that is provided at its 
supports.  Fixity can be assumed where framing members are either continuous through the 
support or attached to a supporting section with a connection bracket.  In other cases, simple 
support is to be assumed unless the connection can be demonstrated to provide significant 
rotational restraint. Fixity is to be ensured at the support of any framing which terminates 
within an ice-strengthened area. 

I2.5.4 The details of framing member intersection with other framing members, including 
plated structures, as well as the details for securing the ends of framing members at 
supporting sections, are to be in accordance with the requirements of the Classification 
Society.  

I2.5.5   The effective span of a framing member is to be determined on the basis of its 
moulded length. If brackets are fitted, the effective span may be reduced in accordance with 
the usual practice of the Classification Society. Brackets are to be configured to ensure 
stability in the elastic and post-yield response regions. 

I2.5.6 When calculating the section modulus and shear area of a framing member, net 
thicknesses of the web, flange (if fitted) and attached shell plating are to be used. The shear 
area of a framing member may include that material contained over the full depth of the 
member, i.e. web area including portion of flange, if fitted, but excluding attached shell 
plating. 

FUnctions:
    frame_shearA_requirements - calculates the frame shear area requirement according to IACS
    frame_Z_requirements - calculates the frame section modulus requirement according to IACS
    frame_design - frame designer, initial seed design is such that satisfies the buckling requirements of IACS

'''
import numpy as np

def frame_shearA_requirements(framing,span_frame,b_patch,PPF_calc,sigma_y,P_avg,AF,s_frame):
    # I2.6
    #Here the simplification is made that bottom is not calculated, only side structure
    # THe design of those needs to meet all the stability requirements as defined in I2.8
    if framing== 'transverse':              #for transversals
        LL=min(span_frame,b_patch)          #[m] lesser of frame span or load patch height
        PPF=PPF_calc[1]
        At=100**2*0.5*LL*s_frame*(AF*PPF*P_avg)/(0.577*sigma_y)     #[cm2]
        Aout=At
    else:  #for longitudinals
        PPF=PPF_calc[2]         #PPFs
        b_prime=b_patch/s_frame             # ratio [m/m]
        if b_prime<2:
            b2=b_patch*(1-0.25*b_prime)     #[m]
        else:
            b2=s_frame                      #[m]   
        k0=1-0.3/b_prime       
        b1=k0*b2                            #[m]                    
        AL=100**2*(AF*PPF*P_avg)*0.5*b1*span_frame/(0.577*sigma_y)     #[cm2]
        Aout=AL
    return(Aout)

def frame_Z_requirements(framing, span_frame,b_patch,PPF_calc,sigma_y,P_avg,AF,s_frame,Aw,A_fn,t_s,b_f,t_f,t_pn,Zp):
    if framing== 'transverse': 
        LL=min(span_frame,b_patch)          #[m] lesser of frame span or load patch height
        PPF=PPF_calc[1]
    # Minimum required section modulus (I2.6.3)
        Y=1-0.5*(LL/span_frame)
        j=2     #local frame without any simple support
        # TODO Other option is to set j=1, but this seems to be less conservative, so 2 is chosen here
        At=frame_shearA_requirements(framing,span_frame,b_patch,PPF_calc,sigma_y,P_avg,AF,s_frame)
        a_1=At/Aw      
        kw=1/(1+2*A_fn/Aw)
        b_eff=500*s_frame                                   #[mm]
        zp=(b_f*(t_f-t_s)**2/4+b_eff*t_pn**2/4)/1000         #[cm3]   
        kz=zp/Zp
        print('kz={:5.2f} '.format(kz))
        if kz<0:
            kz=0
        A1=max(
            1/(1+j/2+kw*j/2*((1-a_1**2)**0.5-1)),
            (1.-1./(2.*a_1*Y))/(0.275+1.44*kz**0.7))
        Zpt=100**3*LL*Y*s_frame*(AF*PPF*P_avg)*s_frame*A1/(4*sigma_y)
        Z0=Zpt
    else:  #for longitudinals
        PPF=PPF_calc[2]         #PPFs
        b_prime=b_patch/s_frame             # ratio [m/m]
        if b_prime<2:
            b2=b_patch*(1-0.25*b_prime)     #[m]
        else:
            b2=s_frame                      #[m]   
        k0=1-0.3/b_prime       
        b1=k0*b2                            #[m] 
        AL=frame_shearA_requirements(framing,span_frame,b_patch,PPF_calc,sigma_y,P_avg,AF,s_frame)                   
        a_4=AL/Aw
        k_wl=1/(1+2*A_fn/Aw)
        A_4=1/(2+k_wl*((1-a_4**2)**0.5-1))
        ZpL=100**3*(AF*PPF*P_avg)*b1*span_frame**2*A_4/(8*sigma_y)  
        Z0=ZpL
    return(Z0)



def frame_design(framing,span_frame,b_patch,PPF_calc,sigma_y,P_avg,AF,s_frame,t_s,t_net):
# % web buckling restriction for bulb,tee and angle sections
# % However, according to IACS these requirements are not practical for
# % stringers or webframes.

    A0=frame_shearA_requirements(framing,span_frame,b_patch,PPF_calc,sigma_y,P_avg,AF,s_frame)
    # shear_A_ratio=1
    #TODO hwtwn only defined for T bars or bulb profiles,
    hwtwn=805/np.sqrt(sigma_y)-0.1      #hw/tw  must be less or equal to satisfy the buckling requirement (0.2 is safety margin)
    t_pn=np.ceil(t_net)                 # [mm]  fitted net shell plate thickness (complying with tnet as required by I2.42) 
    t_wn=0.35*t_pn*(sigma_y/235)**0.5   # [mm] net web thickness requirement (IACS I2.9.3)
    tftw=7/10                           #flange t / web t  15/10 original (IACS)
    bftw=8/1                            #wf/tf  flange width / web thickness 80/10 original
    Aw=1    #starting condition
    Zp=1    #starting condition
    Zreq=2  #starting condition
    it=0
    while Aw < A0 or Zp < Zreq:
        print('Frame iteration={:3.0f} '.format(it))        
    # while Aw < A0:
        # shear_A_ratio=shear_A_ratio-0.01 
        #    
        # Aa=A0/
        h_w=np.ceil(hwtwn*t_wn)                  # [mm] web height
        t_w=np.ceil(t_wn+t_s)                    # [mm] web thickness
        b_f=np.ceil(bftw*t_wn)                   # [mm] flange length
        t_f=np.ceil(tftw*t_w)                    # [mm] flange thickness
        h_stiff=h_w+t_f                 # [mm] web+flange height of the stiffener 
        fii_w=np.deg2rad(90)            # smallest angle between shell plate and stiffener web (deg)
        # Section I2.5.7 
        # The actual net effective shear area, Aw, of a transverse or longitudinal local frame is given by: 
        Aw=h_stiff*t_wn*np.sin(fii_w)/100   #[cm2]
        if Aw < A0: #this helps to avoid excessive calls. If the shear area requirement is not satisfied, go to next iteration
            t_wn+=0.5
            it+=1
            continue
        A_fn=b_f*t_f/100                    #[cm2] Area of frame flange
        A_frame=h_w*t_wn+b_f*t_f            #[mm2]
        Apn=A_frame/100                     #[cm2]
        # Plastic section modulus (actual)
        h_fc=h_w+t_f/2                      #[mm]
        b_w=b_f/2                           #[mm]
        if t_pn*s_frame > A_frame:
            Zp=Apn*t_pn/20+h_w**2*t_wn*np.sin(fii_w)/2000+A_fn*(h_fc*np.sin(fii_w)-b_w*np.cos(fii_w))/10    #[cm3]
        else:
            z_na=(100*A_fn+h_w*t_wn-1000*t_pn*s_frame)/(2*t_wn)     # plastic netutral axes [mm]
            Zp=t_pn*s_frame*(z_na+t_pn/2)*np.sin(fii_w)+(((h_w-z_na)**2+z_na**2)*t_wn*np.sin(fii_w)/2000+A_fn*((h_fc-z_na)*np.sin(fii_w)-b_w*np.cos(fii_w))/10)  #[cm3]
        # Plastic section modulus (required)
        Zreq=frame_Z_requirements(framing, span_frame,b_patch,PPF_calc,sigma_y,P_avg,AF,s_frame,Aw,A_fn,t_s,
            b_f,t_f,t_pn,Zp)
        t_wn+=0.5 
        it+=1
    return(h_w,t_w,b_f,t_f,Zp,Zreq,Aw,A0)  #Zp-plastic section modulus,Zreq - required sec modulus, A0-required shear area


