'''
Created by M.Kõrgesaar August 2020

Includes the following functions: 
    stringer_required2 - Calculates stringer requirements based on FSICR. Because of the difference in design point and load application, the FSICR equations are relaxed. Safety factors are taken as 1.   

    webdesign - Calculates webframe requirements based on FSICR. Because of the difference in design point and load application, the FSICR equations are relaxed. Safety factors are taken as 1.  

    def design_T_frames - designs girders and webframes including the requirements. If requirements are not satisfied, message is printed. 

'''


import numpy as np
import sys


def stringer_required2(p,b_patch,w_patch,span_stringer,s_stringer,m,sigma_y):
    '''
    # This follows FSICRs, but is made more reasonable based on the approach explained in notes of Mikkos paper (goodnotes)
    '''
    # First,split the load height to components to account load distribution to adjacent stringers
# -v1
    if b_patch>s_stringer:     #stringer spacing is used
        h=s_stringer           #in this case some of the load from the patch will be carried by other stringers
    else:
        h=b_patch
# -v2 14.08.2020
    # load height is normalized to get more less equivalent designs between 
    # PC5 and IA Super class. In IA super load height is 0.35m. 
    # h=s_stringer/2
    #Line load
#---------------------
# v01-13.08.2020 Here the input pressure is nominal value
    #TODO This 3.5 is correction factor for the pressure to align the FSICR design with IACS
    # w=h*p/3.5
# v02-17.08.2020 Here the input pressure is scaled by area factor AF
# calculation of the line load, so w here is not the width, but lineload with units of N/m
    w=h*p    
#---------------------
    # 2. Define the load width (either patch width or beam length, which equals span of the stringer)
    if w_patch>span_stringer:
        w_patch=span_stringer       #to avoid situation where patch is wider than the beam   
    beam_L=span_stringer
        
    #Calculate the moment for beam carrying a distributed load located symmetrically in the middle
    M_mid=w_patch*w*beam_L/4-w*w_patch**2/8     #Eq(1)for distributed loading in the middle (see goodnotes)
    # M_full=w*h*beam_L**2/8                    #Eq(2)
    # 
    #in Section modulus calculation Mmid is used here as Mmid==Mfull when the load length=beam length.
    Z0=M_mid/(sigma_y)*10**6          #[cm3]  
    Z0_2=0.9*p*0.35*span_stringer**2*10**6/(13.3*sigma_y)
    
    f6=0.9   #9ditribution of load to adjacent frames = 0.9
    # f7=1.8  #stringer safety factor =1.8
    f7=1.  #stringer safety factor =1.8
    f8=1.  #shear force vs load location =1.2
    # print('p*h=',p*h)
    # print('p= {:4.3f}, h={:4.3f}, l={:4.3f}, Z0={:4.3f} \n'.format(p,h,span_stringer,Z0))
    # f7=1.8
    # # p-pressure MPa
    # # h-load height m
    # # p*h not less than 0.15
    # # l - stringer span
    # # m - boundary condition factor 4.4.3, m=13.3
    # # f5 - ditribution of load to adjacent frames = 0.9
    # # f7 - stringer safety factor =1.8
    # # f8 - shear force vs load location =1.2
    # # sigma_y - yield stress
    # if ph < 0.15:
    #     ph=0.15
    

    # # Effective shear area of the stringer [cm2]
    A0=np.sqrt(3)*f6*f7*f8*w*span_stringer*10**4/(2*sigma_y)
    return(A0,Z0)

def webdesign(p,b_patch,s_webframe,s_stringer,span_web,sigma_y,hw,tw,wf,tf):
    f12=1.     #webframe safety factor
    f13=1.     #shear force distribution factor
    # Load height
    if b_patch>s_stringer:     #stringer spacing is used
        h=s_stringer           #in this case some of the load from the patch will be carried by other stringers
    else:
        h=b_patch
    #TODO Same assumption as in stringer design to scale the initial pressure.
#-------------------------------------------------------------
# v01-13.08.2020 Here the input pressure is nominal value
    # ca=0.35
# TODO v02-17.08.2020 Here the input pressure is scaled by area factor AF. That is not entirely correct to scale because input pressure is already scaled by IACS rules. However, later on in moment calculation i used the webframer span (span_web), but actual equation uses stringer span.  Span_web used here was 6 m, which is ~3x higher than stringer_span
    w=h*p*0.35
#-------------------------------------------------------------

    # load from stringers
    F=f12*w*s_webframe
    # print("Force web   = {:5.1f} [MN]\n".format(F))  
    Q=1.*F
# % determination of alfa and gamma
    alfa=np.array([1.5, 1.23, 1.16, 1.11, 1.09, 1.07, 1.06, 1.05, 1.05, 1.04, 1.04])
    gamma=np.array([0, 0.44, 0.62, 0.71, 0.76, 0.8, 0.83, 0.85, 0.87, 0.88, 0.89])
    aratio=np.arange(0,2.2,0.2)

    Af=wf*tf           #(in cm now)
    Aw=tw*hw           #(in cm now)
    Aa=Af+Aw           #(in cm now)
    ratio=Af/Aw        #(in cm now)
    alfa_d=np.interp(ratio,aratio,alfa)
    gamma_d=np.interp(ratio,aratio,gamma)

    # %Effective shear area requirment
    Areq=np.sqrt(3)*alfa_d*f13*Q*10**4/sigma_y       #cm2

    # %Section modulus requirement
    # FIXME span_web should be replaced with span of stringers. If you correct this fix also this equations w=h*p*0.35
    M=0.193*F*span_web
    Zweb=M/sigma_y*np.sqrt(1/(1-(gamma_d*Areq/Aa)**2))*10**6       #cm2
    return(Areq,Zweb)


def design_T_frames(element,hw,tw,wf,tf,s,tp,sigma_y,Z0,A0):
    # # Z0 min section modulus reuqirement cm3
    # # A0 min shear area requirement cm2
    # # effective breadth of the girder
    # # tp plate thickness as fitted in mm
    if hw/tw> (805/np.sqrt(sigma_y)):         #web thickness
        print('{:1} height/web ratio does not fulfill buckling requirements \n'.format(element))
    # # Section modulus
    # # ----------------------
    # # web calculations
    Aw=hw*tw                    #mm**2  web area
    COGw=hw/2                   #mm    center of gravity
    Iw=(tw*hw**3)/12             #mm**4  2nd moment of area
    #flange calculations
    Af=wf*tf                    #mm**2
    COGf=tf/2                   #mm
    If=(wf*tf**3)/12             #mm**4
    #plate calculations
    Ap=tp*s                     #mm**2
    COGp=tp/2                   #mm
    Ip=s*tp**3/12                #mm**4
    #Elastic section modulus
    NA=(Aw*(tp+hw/2)+Af*(tp+hw+tf/2)+(Ap*tp/2))/(Aw+Af+Ap) #mm
    TotI=Iw+If+Ip+(Aw*(COGw+tp-NA)**2)+(Af*(hw+tp+COGf-NA)**2)+(Ap*(COGp-NA)**2)#mm**4
    Z1=TotI/(hw+tf+tp-NA)/1000   #cm3
    Z2=TotI/(NA)/1000
    Ze=min(Z1,Z2)   #cm3
    # -----------------------
    Aa=(Aw+Af)/100
    if (Aa<A0) or (Ze<Z0):         #web thickness
        print('{:5} does not fulfill the requirements Z0={:6.2f} A0={:6.1f}'.format(element,Z0,A0))
        print('Current {:5} Ze={:6.2f} Aa={:6.1f}'.format(element,Ze,Aa))
        # sys.exit('Redesing {:5}'.format(element))
    else:
        print('{:5} Ze({:6.2f}) > Z0({:6.2f}), Aa({:6.1f})>A0({:6.1f})\n'.format(element,Ze,Z0,Aa,A0))
    return(Ze,Aa)

if __name__ == "__main__": 
    sigma_y=285
    hw_s=700     #webheight
    tw_s=20      #web thickness
    
    if hw_s/tw_s> (805/np.sqrt(sigma_y)):         #web thickness
        print('Stringer height/web ratio does not fulfill buckling requirements \n'.format(p,h,span_stringer,Z0))
    wf_s=140     #flange width
    tf_s=30     #flange thickness
    t_net=22
    t_s=2.5  
    ratio=hw_s/tw_s
    hwtwn=(805/np.sqrt(sigma_y))
    sigma_y=285
    span_stringer=s_stringer=2.8
    span_web=3
    s_frame=0.4
    (A0_stringer2,Z0_stringer2)=stringer_required2(3.34,1.48,2.3,span_stringer,s_stringer*0.7,13.3,sigma_y)
    (Ze,Aa)=design_T_frames(hw_s,tw_s,wf_s,tf_s,s_frame,t_net+t_s)
    # IACS=1000*p*s