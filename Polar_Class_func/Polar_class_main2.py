'''
Original version Created by M.Kõrgesaar August 2020

This is the test version appended with _2

The purpose was to check how the safety factors affect the design. In the webframe and stringer calculations
this runs F03_webs_and_stringers2 where safety factors are taken to unity. 

Corresponding results are written to file *_testversion. (e.g. PC3_bow_Indx_1-BI_I_testversion)

Example:
Original results (PC3_bow_Indx_1-BI_I)
-----Stringer requirements and Design--------
Z_0= 10528.6 [cm3] 
A_0= 303.9 [cm2] 
Designed stingers
Z_s= 7754.5 [cm3] 
A_s= 224.0 [cm2] 
hwxtw + wfxtf=  800x22 + 200x24 [mm] 

-----Webframe requirements and Design--------
Z_0= 17560.9 [cm3] 
A_0= 310.8 [cm2] 
Designed webframes
Z_s= 12217.3 [cm3] 
A_s= 300.0 [cm2] 
hwxtw + wfxtf=  900x25 + 300x25 [mm] 


Testversion: (safety 1)
-----Stringer requirements and Design--------
Z_0= 6333.0 [cm3] 
A_0= 337.7 [cm2] 
Designed stingers
Z_s= 7754.5 [cm3] 
A_s= 224.0 [cm2] 
hwxtw + wfxtf=  800x22 + 200x24 [mm] 

-----Webframe requirements and Design--------
Z_0= 11707.2 [cm3] 
A_0= 207.2 [cm2] 
Designed webframes
Z_s= 12217.3 [cm3] 
A_s= 300.0 [cm2] 
hwxtw + wfxtf=  900x25 + 300x25 [mm] 
'''

import pandas as pd
import numpy as np
import os
import subprocess
import sys
import F01_load_calc2 as fun_load
import F02_framing2 as fun_frames
import F03_webs_and_stringers2 as fun_webs
print(__name__)
# print(sys.executable)
# TODO
# Definitions
Lui =122  # Length in mz
B = 22  # Breadth in m
D = 7.65  # Draft in m
DWT = 5000  # Deadweight in ton
P = 12000  # Power in kw
Cb = 0.7  # Block coefficient
roo = 1  # density ton/m3
disp = roo*Lui*B*D*Cb  # Displacement in ton
Dui = disp/1000  # Displacement in kton
PC = 3 # Polar Class

Region = 'bow'  # Hull region (bow or nobow)
Region_spec_ind=1
region_list=['0-B', '1-BI_I', '2-BI_L', '3-BI_B', '4-M_I', '5-M_L', '6-M_B', '7-S_I', '8-S_L', '9-S_B']
    #index is shown before the column name for quick access
    #The picture with regions is shown in Figure 1 of PC class rules.
    # B=bow,BI_I=Bow intermediate-ice Belt, BI_L=Bow intermediate-lower,BI_B=Bow intermediate-bottom
    # M=Midbody, ice Belt, lower,bottom
    # S=Stern, ice Belt, lower,bottom

# These are important only for bow shape. If we design midbody, then not used
alfa = 19  # Upper ice Waterline angle [deg]
gamma = 65  # Buttock angle [deg]
beta_p = 12  # beta_p prime, normal frame angle at upper ice waterline [deg]

# Framing
framing = 'transverse'  # 'bottom', 'longitudinal', 'transverse'
stringer = 'yes'
s_frame = 0.4              #frame spacing
s_stringer=1.5             #stringer spacing [m]
span_frame=s_stringer      #[m] See I2.5.5, support point for frames
s_webframe = 2.4            #webframe spacing
span_web=6                  #webframe support points
span_stringer=s_webframe    #assume that stringers are supported by webframes (can be multiplied by 0.7 to account brackets)

sigma_y=355                 #Mateiral yield stress MPa



# def input_uikku():
    # Lui = 213  # Length in m
    # B = 17.5  # Breadth in m
    # D = 8.2  # Draft in m
    # DWT = 8145  # Deadweight in ton
    # P = 11400  # Power in kw
    # Cb = 0.7  # Block coefficient
    # roo = 1  # density ton/m3
    # disp = roo*Lui*B*D*Cb  # Displacement in ton
    # Dui = disp/1000  # Displacement in kton
    # PC = 1  # Polar Class

    # Region = 'bow'  # Hull region (bow or nobow)
    # Region_spec_ind=1
    # # region_list=['0-B', '1-BI_I', '2-BI_L', '3-BI_B', '4-M_I', '5-M_L', '6-M_B', '7-S_I', '8-S_L', '9-S_B']
    #     #index is shown before the column name for quick access
    #     #The picture with regions is shown in Figure 1 of PC class rules.
    #     # B=bow,BI_I=Bow intermediate-ice Belt, BI_L=Bow intermediate-lower,BI_B=Bow intermediate-bottom
    #     # M=Midbody, ice Belt, lower,bottom
    #     # S=Stern, ice Belt, lower,bottom
    # alfa = 19  # Upper ice Waterline angle [deg]
    # gamma = 65  # Buttock angle [deg]
    # beta_p = 12  # beta_p prime, normal frame angle at upper ice waterline [deg]

    # # Framing
    # framing = 'transverse'  # 'bottom', 'longitudinal', 'transverse'
    # stringer = 'yes'
    # s_frame = 0.3              #frame spacing
    # s_stringer=2.2             #stringer spacing [m]
    # span_frame=s_stringer      #[m] See I2.5.5, support point for frames
    # s_webframe = 2.            #webframe spacing
    # span_web=3                  #webframe support points
    # span_stringer=s_webframe    #assume that stringers are supported by webframes (can be multiplied by 0.7 to account brackets)

    # sigma_y=285                 #Mateiral yield stress MPa


# Start of the calculations
'''
Hull areas
I2.2.1   The hull of Polar Class ships is divided into areas reflecting the magnitude of the 
loads that are expected to act upon them. In the longitudinal direction, there are four regions: 
Bow, Bow Intermediate, Midbody and Stern. The Bow Intermediate, Midbody and Stern 
regions are further divided in the vertical direction into the Bottom, Lower and Icebelt regions. 
The extent of each hull area is illustrated in Figure 1. 
'''

CF = pd.DataFrame(np.array([[17.69, 68.60, 2.01, 250, 7.46],
                            [9.89, 46.80, 1.75, 210, 5.46],
                            [6.06, 21.17, 1.53, 180, 4.17],
                            [4.50, 13.48, 1.42, 130, 3.15],
                            [3.10, 9.00,  1.31, 70, 2.50],
                            [2.40, 5.49,  1.17, 40, 2.37],
                            [1.80, 4.06,  1.11, 22, 1.81]]),
                  columns=['C', 'F', 'D', 'DIS', 'L'],
                  index=[1, 2, 3, 4, 5, 6, 7])
CF2 = pd.DataFrame(np.array([[3.43, 2.82, 0.65],
                             [2.60, 2.33, 0.65]]),
                   columns=['CV', 'QV', 'PV'],
                   index=[6, 7])
# CF2.CV[6]
# Calculation of hull area factors
AF_icebreaker_table = pd.DataFrame(np.array([[1.,0.9,0.7,0.55,0.7,0.5,0.3,0.95,0.55,0.35],
               [1., 0.85, 0.65,  0.5, 0.65, 0.45, 0.3, 0.9, 0.5,  0.3],
               [1., 0.85, 0.65, 0.45, 0.55, 0.4, 0.25, 0.8, 0.45, 0.3],
               [1., 0.85, 0.65, 0.45, 0.55,0.4, 0.25, 0.8, 0.45, 0.3],
               [1., 0.85, 0.65, 0.45, 0.55, 0.4, 0.25, 0.8, 0.45, 0.3],
               [1., 1., 0.65, 0.45, 0.55,  0.4, 0.25, 0.8, 0.45,  0.3],
               [1., 1., 0.65, 0.45, 0.55,  0.4, 0.25, 0.8, 0.45,  0.3]]),
               columns=['0-B', '1-BI_I', '2-BI_L', '3-BI_B', '4-M_I', '5-M_L', '6-M_B', '7-S_I', '8-S_L', '9-S_B'],  #index is shown before the column name for quick access
               index=[1, 2, 3, 4, 5, 6, 7])
AF_table= pd.DataFrame(np.array([[1.,0.9, 0.7,0.55,0.7,0.5,0.3,0.75,0.45,0.35],
               [1., 0.85, 0.65,  0.5, 0.65, 0.45, 0.3, 0.7, 0.4,  0.3],
               [1., 0.85, 0.65, 0.45, 0.55, 0.4, 0.25, 0.65, 0.35, 0.3],
               [1., 0.8, 0.6, 0.4, 0.55,0.35,np.nan, 0.6, 0.3, 0.25],
               [1., 0.8, 0.55, 0.35, 0.5,0.3,np.nan , 0.5, 0.25, 0.15],
               [1., 1, 0.55, 0.3, 0.45,0.25, np.nan, 0.4, 0.25, np.nan ],
               [1., 1, 0.5, 0.25, 0.45,0.25, np.nan, 0.35, 0.25,np.nan ]]),
               columns=['0-B', '1-BI_I', '2-BI_L', '3-BI_B', '4-M_I', '5-M_L', '6-M_B', '7-S_I', '8-S_L', '9-S_B'],
               index=[1, 2, 3, 4, 5, 6, 7])
#I2.11.3   Polar Class ships are to have a minimum corrosion/abrasion addition of ts = 1.0 mm applied to all internal structures within the ice-strengthened hull areas, including plated members adjacent to the shell, as well as stiffener webs and flanges
ts_table= pd.DataFrame(np.array([
       [3.5, 2.5, 2. ],
       [3.5, 2.5, 2. ],
       [3.5, 2.5, 2. ],
       [2.5, 2. , 2. ],
       [2.5, 2. , 2. ],
       [2. , 2. , 2. ],
       [2. , 2. , 2. ]]),
       columns=['Bow; Bow Intermediate; Icebelt','Bow Intermediate Lower; Midbody & Stern Icebelt ','Midbody & Stern Lower; Bottom'],
       index=[1, 2, 3, 4, 5, 6, 7])

#Some parameters calculated according to above tables
# AF=AF_table.BI_I[PC]       # To calculate for some other region than bow then letter B needs to be changed to respective location
AF=AF_table.loc[PC][Region_spec_ind] 
t_s=ts_table.loc[PC][0]     #logic: row index is PC class (1-7) and column index is standard python 0-2
# -------------------------------------------------------------------------------------------------

# Calculation of Load related variables for stiffeners
(w_patch,b_patch,P_avg)=fun_load.load_calc(Region,PC,gamma,beta_p,Lui,alfa,Dui,CF,CF2)
# b_patch=patch height
# w_patch=patch width
PPF_calc=fun_load.PPF(framing, s_frame, s_webframe, w_patch, stringer)


 

# ----------------------------------------------------------------------------- 
# I2.4 Shell plate requirements
if framing == 'transverse':
    PPFp=PPF_calc[0]
    t_net=500*s_frame*((AF*PPFp*P_avg)/sigma_y)**0.5/(1+s_frame/(2*b_patch))        # [mm]
elif framing == 'longitudinal':
    PPFp=PPF_calc[0]
    if b_patch>= s_frame:
    # Here the span_frame should not be reduced due to end brackets (see I2.4.2)
        t_net=500*s_frame*((AF*PPFp*P_avg)/sigma_y)**0.5/(1+s_frame/(2*span_frame))        # [mm]
    else:
        t_net=500*s_frame*((AF*PPFp*P_avg)/sigma_y)**0.5/(2*b_patch/s_frame-
            (b_patch/s_frame)**2)**0.5/(1+s_frame/(2*span_frame))        # [mm]

print('tnet=',"{:5.1f} [mm] \n".format(t_net))

'''
Framing
I2.5.1 Framing members of Polar Class ships are to be designed to withstand the ice loads defined in I2.3. 
'''
A0=fun_frames.frame_shearA_requirements(framing,span_frame,b_patch,PPF_calc,sigma_y,P_avg,AF,s_frame)
(h_w,t_w,b_f,t_f,Zp,Zreq,Aw,A0)=fun_frames.frame_design(framing,span_frame,b_patch,PPF_calc,sigma_y,P_avg,AF,s_frame,t_s,t_net)

'''
# Design of webs and stringers
'''



(_,_,PPF_web)=fun_load.PPF('stringer', s_frame, s_webframe, w_patch, stringer)
# b_patch=patch height
# w_patch=patch width
# (A0_stringer,Z0_stringer)=fun_webs.stringer_required(P_avg*PPF_web,b_patch,span_stringer,13.3,sigma_y)
pressure_stringer=P_avg*PPF_web*AF
(A0_stringer,Z0_stringer)=fun_webs.stringer_required2(pressure_stringer,b_patch,w_patch,0.9*span_stringer,s_stringer,13.3,sigma_y)


Stringer_scant = pd.DataFrame(np.array([[[1,1,2,3],0.9,0.7,0.55,0.7,0.5,0.3,0.95,0.55,0.35],
               [1., 0.85, 0.65,  0.5, 0.65, 0.45, 0.3, 0.9, 0.5,  0.3],
               [1., 0.85, 0.65, 0.45, 0.55, 0.4, 0.25, 0.8, 0.45, 0.3],
               [1., 0.85, 0.65, 0.45, 0.55,0.4, 0.25, 0.8, 0.45, 0.3],
               [1., 0.85, 0.65, 0.45, 0.55, 0.4, 0.25, 0.8, 0.45, 0.3],
               [1., 1., 0.65, 0.45, 0.55,  0.4, 0.25, 0.8, 0.45,  0.3],
               [1., 1., 0.65, 0.45, 0.55,  0.4, 0.25, 0.8, 0.45,  0.3]]),
               columns=['0-B', '1-BI_I', '2-BI_L', '3-BI_B', '4-M_I', '5-M_L', '6-M_B', '7-S_I', '8-S_L', '9-S_B'],  #index is shown before the column name for quick access
               index=[1, 2, 3, 4, 5, 6, 7])

Stringer_scant2 = pd.DataFrame(np.array([[1,[1,2,3]],[1,3]]),
               columns=['Bow', 'Nobow'])  #index is shown before the column name for quick access)
# ----STRINGERS------
if Region=='nobow':
    if PC==1:
        # PC5
        tw1=30   #web thickness [mm]
        hw1=1200   #webheight [mm]
        wf1=400   #flange width [mm]
        tf1=34   #flange thickness [mm]
    if PC==2:
        # PC5
        tw1=28   #web thickness [mm]
        hw1=800   #webheight [mm]
        wf1=200   #flange width [mm]
        tf1=28   #flange thickness [mm]
    if PC==3:
        # PC3 midsection
        tw1=20   #web thickness [mm]
        hw1=600   #webheight [mm]
        wf1=140   #flange width [mm]
        tf1=22   #flange thickness [mm]
        # # PC3 stern
        # tw1=20   #web thickness [mm]
        # hw1=600   #webheight [mm]
        # wf1=140   #flange width [mm]
        # tf1=22   #flange thickness [mm]
    if PC==4:
        # PC5
        tw1=20   #web thickness [mm]
        hw1=660   #webheight [mm]
        wf1=160   #flange width [mm]
        tf1=22   #flange thickness [mm]
    if PC==5:
        # PC5
        tw1=18   #web thickness [mm]
        hw1=500   #webheight [mm]
        wf1=120   #flange width [mm]
        tf1=20   #flange thickness [mm]
    elif PC==6:
        # PC6
        tw1=14   #web thickness [mm]
        hw1=500   #webheight [mm]
        wf1=80   #flange width [mm]
        tf1=16   #flange thickness [mm]
    elif PC==7:
        # PC6
        tw1=14   #web thickness [mm]
        hw1=400   #webheight [mm]
        wf1=80   #flange width [mm]
        tf1=16   #flange thickness [mm]
# ----STRINGERS------

#Bow intermediate ice belt (stringers)
if Region=='bow' and Region_spec_ind==1:
    print('Region_spec_ind==1')
    if PC==1:
        # PC5
        tw1=30   #web thickness [mm]
        hw1=1200   #webheight [mm]
        wf1=400   #flange width [mm]
        tf1=34   #flange thickness [mm]
    if PC==2:
        # PC5
        tw1=28   #web thickness [mm]
        hw1=800   #webheight [mm]
        wf1=200   #flange width [mm]
        tf1=28   #flange thickness [mm]
    if PC==3:
        # PC3 one of the analyzed cases--------------------------------------
# these are reduced scantlgins (not compliying with requirements)        
        tw1=22   #web thickness [mm]
        hw1=800   #webheight [mm]
        wf1=200   #flange width [mm]
        tf1=24   #flange thickness [mm]
# these are original scantlings (compliying with requirements)
        # tw1=26   #web thickness [mm]
        # hw1=900   #webheight [mm]
        # wf1=300   #flange width [mm]
        # tf1=26   #flange thickness [mm]
    if PC==4:
        # PC5
# compliant    
        mpc=0.9    
        tw1=int(22*mpc)   #web thickness [mm]
        hw1=int(800*mpc)   #webheight [mm]
        wf1=int(220*mpc)   #flange width [mm]
        tf1=int(24*mpc)   #flange thickness [mm]
    if PC==5:
        mpc=1    
        tw1=int(21*mpc)   #web thickness [mm]
        hw1=int(720*mpc)   #webheight [mm]
        wf1=int(180*mpc)   #flange width [mm]
        tf1=int(21*mpc)   #flange thickness [mm]
    elif PC==6:
        # PC6
        mpc=1  
        tw1=int(20*mpc)   #web thickness [mm]
        hw1=int(700*mpc)   #webheight [mm]
        wf1=int(160*mpc)   #flange width [mm]
        tf1=int(20*mpc)   #flange thickness [mm]
    elif PC==7:
        # PC6
        tw1=20   #web thickness [mm]
        hw1=660   #webheight [mm]
        wf1=140   #flange width [mm]
        tf1=20   #flange thickness [mm]


b_effective=s_stringer*0.8*1000     #[mm]  
Tstringers=fun_webs.design_T_frames('stringer',hw1,tw1,wf1,tf1,b_effective,t_net+t_s,sigma_y,Z0_stringer,A0_stringer)
# Tstringers=design_T_frames('stringer',hw1,tw1,wf1,tf1,b_effective,t_net+t_s,sigma_y,Z0_stringer,A0_stringer)


if Region=='nobow':   
    # ----Design webframes----
    if PC==1:
        tw2=34   #web thickness [mm]
        hw2=1400   #webheight [mm]
        wf2=800   #flange width [mm]
        tf2=40   #flange thickness [mm]
        # Z0 has safety factor of 2.
    if PC==2:
        tw2=30   #web thickness [mm]
        hw2=1000   #webheight [mm]
        wf2=400   #flange width [mm]
        tf2=32   #flange thickness [mm]
        # Z0 has safety factor of 2.
    # PC3
    if PC==3:
        #         tw1=20   #web thickness [mm]
        # hw1=600   #webheight [mm]
        # wf1=140   #flange width [mm]
        # tf1=22   #flange thickness [mm]
        tw2=22   #web thickness [mm]
        hw2=700   #webheight [mm]
        wf2=160   #flange width [mm]
        tf2=24   #flange thickness [mm]
    # PC4
    if PC==4:
        tw2=22   #web thickness [mm]
        hw2=710   #webheight [mm]
        wf2=160   #flange width [mm]
        tf2=24   #flange thickness [mm]
    # PC5
    if PC==5:
        tw2=18   #web thickness [mm]
        hw2=620   #webheight [mm]
        wf2=140   #flange width [mm]
        tf2=20   #flange thickness [mm]
    elif PC==6:
        # PC6
        tw2=16   #web thickness [mm]
        hw2=560   #webheight [mm]
        wf2=100   #flange width [mm]
        tf2=18   #flange thickness [mm]
    elif PC==7:
        # PC7
        tw2=16   #web thickness [mm]
        hw2=500   #webheight [mm]
        wf2=90   #flange width [mm]
        tf2=18   #flange thickness [mm]     

# ----Design webframes----
#Bow intermediate ice belt
if Region=='bow' and Region_spec_ind==1:
    print('Region_spec_ind==1')
    if PC==1:
        tw2=34   #web thickness [mm]
        hw2=1400   #webheight [mm]
        wf2=800   #flange width [mm]
        tf2=40   #flange thickness [mm]
        # Z0 has safety factor of 2.
    if PC==2:
        tw2=30   #web thickness [mm]
        hw2=1000   #webheight [mm]
        wf2=400   #flange width [mm]
        tf2=32   #flange thickness [mm]
        # Z0 has safety factor of 2.
    # PC3
    if PC==3:
# non-compliant
        tw2=25   #web thickness [mm]
        hw2=900   #webheight [mm]
        wf2=300   #flange width [mm]
        tf2=25   #flange thickness [mm]
# compliant
        # tw2=30   #web thickness [mm]
        # hw2=1000   #webheight [mm]
        # wf2=420   #flange width [mm]
        # tf2=32   #flange thickness [mm]
    # PC4
    if PC==4:
# compliant    
        mpc=0.9    
        tw2=int(25*mpc)   #web thickness [mm]
        hw2=int(900*mpc)   #webheight [mm]
        wf2=int(400*mpc)   #flange width [mm]
        tf2=int(27*mpc)   #flange thickness [mm]
    # PC5
    if PC==5:
        mpc=1    
        tw2=int(22*mpc)   #web thickness [mm]
        hw2=int(820*mpc)   #webheight [mm]
        wf2=int(300*mpc)   #flange width [mm]
        tf2=int(24*mpc)   #flange thickness [mm]
    if PC==6:
        mpc=1   
        tw2=int(22*mpc)   #web thickness [mm]
        hw2=int(780*mpc)   #webheight [mm]
        wf2=int(260*mpc)   #flange width [mm]
        tf2=int(24*mpc)   #flange thickness [mm]
    if PC==7:
        mpc=1   
        tw2=int(22*mpc)   #web thickness [mm]
        hw2=int(700*mpc)   #webheight [mm]
        wf2=int(260*mpc)   #flange width [mm]
        tf2=int(24*mpc)   #flange thickness [mm]

b_effective=s_webframe*0.6*1000     #[mm]  
pressure_webs=P_avg*PPF_web*AF
(A0_webframe,Z0_webframe)=fun_webs.webdesign(pressure_webs,b_patch,s_webframe,s_stringer,span_web,sigma_y,hw2,tw2,wf2,tf2)
T_webframes=fun_webs.design_T_frames('Webframe',hw2,tw2,wf2,tf2,b_effective,t_net+t_s,sigma_y,Z0_webframe,A0_webframe)





n=open('PC{0}_{1}_Indx_{2}_testversion.txt'.format(PC,Region,region_list[Region_spec_ind]),"w")                      
n.write('------Input-----\n')
n.write("Class           = PC{0}\n".format(PC))  
n.write("frame spacing   = {0} [m]\n".format(s_frame))  
n.write("webframe spacing= {0} [m]\n".format(s_webframe))  
n.write("frame span      = {0} [m]\n".format(span_frame))  
n.write("Displacement    = {0} [ton]\n".format(np.ceil(disp)))  
n.write("Power           = {0} [kw]\n".format(P))  
n.write("Yield stress    = {0} [MPa]\n".format(sigma_y))  
n.write("Region          = {0}\n".format(Region))  
n.write("\n")  
n.write('IACS requirements\n')                          
n.write('----------------------\n')
n.write("Z0 frames= {:5.1f} [cm3] \n".format(Zreq))  
n.write("A0 frames= {:5.1f} [cm2] \n".format(A0)) 
n.write("t plate= {:5.1f} [mm] \n".format(t_net+t_s)) 
n.write("Pressure= {:5.1f} [MPa] \n".format(P_avg)) 
n.write("Area factor= {:4.2f} [-] \n".format(AF)) 
n.write("PPF plate= {:5.1f} \n".format(PPF_calc[0])) 
n.write("PPF stringer= {:5.1f} \n".format(PPF_calc[2])) 
n.write("Patch wxh={:4.2f} x {:4.2f} [m] ={:5.2f} [m2]\n".format(w_patch,b_patch,w_patch*b_patch)) 
n.write("Total F={:4.2f} [MN] \n".format(w_patch*b_patch*P_avg*AF)) 

n.write('-----Designed T frames-----------------\n')
n.write("Z= {:5.1f} [cm3] \n".format(Zp))   
n.write("A= {:5.1f} [cm2] \n".format(Aw)) 
n.write("hwxtw + wfxtf={:5.0f}x{:2.0f} + {:3.0f}x{:2.0f} [mm] \n".format(h_w,t_w,b_f,t_f))   
n.write("\n") 
n.write('-----Stringer requirements and Design--------\n')
n.write("Z_0= {:5.1f} [cm3] \n".format(Z0_stringer))   
n.write("A_0= {:5.1f} [cm2] \n".format(A0_stringer))   
n.write('Designed stingers\n')
n.write("Z_s= {:5.1f} [cm3] \n".format(Tstringers[0]))   
n.write("A_s= {:5.1f} [cm2] \n".format(Tstringers[1])) 
n.write("hwxtw + wfxtf={:5.0f}x{:2.0f} + {:3.0f}x{:2.0f} [mm] \n".format(hw1,tw1,wf1,tf1))  
n.write("\n")  
n.write('-----Webframe requirements and Design--------\n')
n.write("Z_0= {:5.1f} [cm3] \n".format(Z0_webframe))   
n.write("A_0= {:5.1f} [cm2] \n".format(A0_webframe))   
n.write('Designed webframes\n')
n.write("Z_s= {:5.1f} [cm3] \n".format(T_webframes[0]))   
n.write("A_s= {:5.1f} [cm2] \n".format(T_webframes[1]))   
n.write("hwxtw + wfxtf={:5.0f}x{:2.0f} + {:3.0f}x{:2.0f} [mm] \n".format(hw2,tw2,wf2,tf2))   
n.close()   


n=open('PC{0}_{1}_Indx_{2}_SIMINPUT.txt'.format(PC,Region,region_list[Region_spec_ind]),"w")                      
n.write("webspacing={0}\n".format(s_webframe))  
n.write("stiffeners={:2d}\n".format(int(np.ceil(s_webframe/s_frame)-1)))
n.write("hframe={:5.4f}\n".format(h_w/1000))                #in [m]
n.write("half_flange={:5.4f}\n".format(b_f/1000/2))         #in [m]
n.write("t_frame={:5.4f}\n".format(t_w/1000))               #in [m]
n.write("t_flange={:5.4f}\n".format(t_f/1000))              #in [m]
n.write("tp={:5.4f}\n".format((t_net+t_s)/1000))            #in [m]
n.write("#Stringers---------------------------\n")           
n.write("girderH={:5.4f}\n".format(hw1/1000))           
n.write("girder_flange={:5.4f}\n".format((wf1/2/1000)))  
n.write("tgirder={:5.4f}\n".format(tw1/1000))           
n.write("tgirder2={:5.4f}\n".format(tf1/1000))  
n.write("depth={:5.4f}\n".format(s_stringer))  
n.write("#Webframes---------------------------\n")         
n.write("webheight={:5.4f}\n".format(hw2/1000))                                     #hw
n.write("webframe_flange={:5.4f}\n".format(wf2/2/1000))                          #wf
n.write("tweb={:5.4f}\n".format(tw2/1000))                                         #tw
n.write("tweb2={:5.4f}\n".format(tf2/1000))                                        #ft
n.write("#Loading---------------------------\n") 
n.write("patch_width={:5.4f}\n".format(w_patch))  
n.write("patch_height={:5.4f}\n".format(b_patch))  
n.write("patch_load={:5.4f}\n".format(w_patch*b_patch*P_avg*AF*10**6))    #[N]
n.write("rule_pressure= {:5.1f}\n".format(P_avg*AF*10**6)) #Pa

n.close()        
      
      
      
      
