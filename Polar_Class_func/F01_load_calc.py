'''
Created by M.Kõrgesaar August 2020
# %% I2.3.2.1  Bow area


# The influence of the hull angles is captured through calculation of a bow shape coefficient (fa).
# beta_p - normal frame angle at upper ice waterline [deg]
# alfa -  upper ice waterline angle [deg]
# gamma - buttock angle at upper ice waterline (angle of buttock line measured from horizontal) [deg]
# x = distance from the fore side of the stem at the intersection with the upper ice waterline (UIWL) to station under consideration [m]


# (iii) The Bow area load characteristics for bow forms defined in I2.3.1 (v) are determined as follows:
# (v)   Design ice forces calculated according to I2.3.2.1 (iii) are applicable for bow forms where
# the buttock angle gamma at the stem is positive and less than 80 deg, and the normal frame angle
# beta_p’ at the centre of the foremost sub-region, as defined in I2.3.2.1 (i), is greater than 10 deg.

functions:
    load_calc - load calculations according to IACS
    PPF - Calculation of PPF (peak pressure factor) according to IACS

#TODO 
'''
import numpy as np

def load_calc(Region,PC,gamma,beta_p,Lui,alfa,Dui,CF,CF2):
    if Region == 'bow':
        if gamma < 80 and beta_p > 10 and PC < 6:
            # print('Calculation according to iii')
            x = 20                                # [m]
            # Shape coefficient
            fa1 = (0.097-0.68*(x/Lui-0.15)**2)*alfa/np.sqrt(beta_p)
            fa2 = 1.2*CF.F[PC]/(np.sin(np.deg2rad(beta_p))*CF.C[PC]*Dui**0.64)
            fa3 = 0.6
            fa = min(fa1, fa2, fa3)

            # Force Fi [MN]
            F = fa*CF.C[PC]*Dui**0.64             # [MN]

            # Load patch aspect ratio AR
            AR = 7.46*np.sin(np.deg2rad(beta_p))
            if AR <= 1.3:
                AR = 1.3

            # Line load
            Q = F**0.61*CF.D[PC]/AR**0.35         # [MN/m]

            # Pressure P
            P = F**0.22*CF.D[PC]**2*AR**0.3         # [MPa]

        # The Bow area load characteristics for bow forms defined in I2.3.1 (vi) are determined as follows:
            # (vi)   Design ice forces calculated according to I2.3.2.1 (iv) are applicable for ships which are
            # assigned the Polar Class PC6 or PC7 and have a bow form with vertical sides. This includes
            # bows where the normal frame angles beta_p’ at the considered sub-regions, as defined in I2.3.2.1
            # (i), are between 0 and 10 deg.
        if PC >= 6:
            print('Calculation according to iv')
            fa = alfa/30
            F = fa*CF2.CV[PC]*Dui**0.47           # [MN]
            Q = F**0.22*CF2.QV[PC]                # [MN/m]
            P = F**0.56*CF2.PV[PC]                # [MPa]
    # Design load patch
        w_patch = F/Q  # [m] Load patch width
        b_patch = Q/P  # [m] Load patch height

    # I2.3.2.2 Hull areas other than the bow
    else:
        # DF - ship displacement factor
        if Dui <= CF.DIS[PC]:
            DF = Dui**0.64
        else:
            DF = CF.DIS[PC]**0.64+0.1*(Dui-CF.DIS[PC])
        F = 0.36*CF.C[PC]*DF
        Q = 0.639*F**0.61*CF.D[PC]
    # Design load patch
        w_patch = F/Q  # [m]
        b_patch = w_patch/3.6  # [m]


    '''
    # I2.3.4 Pressure within the design load patch
    '''
    P_avg = F/(b_patch*w_patch)  # [MPa]
    return(w_patch,b_patch,P_avg)

# Calculation of PPF (peak pressure factor)
#PPFp - plating
#PPFt - transversals
#PPFs - longitudinals, webframes, load carrying stringers

def PPF(component, s_frame, s_webframe, w_patch, stringer):
    # For plating (p)
    if component == 'transverse':
        PPFp = max(1.8-s_frame, 1.2)
        if stringer == 'yes':
            PPFt = max(1.6-s_frame, 1)
        else:
            PPFt = max(1.8-s_frame, 1.2)
        PPFs=1
    # For load carrying stringers, side longitudinals and webframes (all under longitudinals)
    elif component == 'longitudinal':
        PPFp = max(2.2-1.2*s_frame, 1.5)
        if s_webframe >= 0.5*w_patch:
            PPFs = 1
        else:
            PPFs = 2.-2*s_webframe/w_patch
        PPFt=1
    elif component =='stringer' or component =='webframe':
        PPFp=1; PPFt=1
        if s_webframe >= 0.5*w_patch:
            PPFs=1
        else:
            PPFs=2-2*s_webframe/w_patch
    return(PPFp,PPFt,PPFs,)