U
    x9�`f   �                   @   s(   d Z ddlZdd� Zdd� Zdd� ZdS )	u!	  
Created by M.Kõrgesaar August 2020

I2.5 Framing - General 

I2.5.1 Framing members of Polar Class ships are to be designed to withstand the ice loads 
defined in I2.3. 

I2.5.2 The term “framing member” refers to transverse and longitudinal local frames, load-
carrying stringers and web frames in the areas of the hull exposed to ice pressure, see Figure 
1. Where load-distributing stringers have been fitted, the arrangement and scantlings of these 
are to be in accordance with the requirements of the Classification Society. 

I2.5.3 The strength of a framing member is dependent upon the fixity that is provided at its 
supports.  Fixity can be assumed where framing members are either continuous through the 
support or attached to a supporting section with a connection bracket.  In other cases, simple 
support is to be assumed unless the connection can be demonstrated to provide significant 
rotational restraint. Fixity is to be ensured at the support of any framing which terminates 
within an ice-strengthened area. 

I2.5.4 The details of framing member intersection with other framing members, including 
plated structures, as well as the details for securing the ends of framing members at 
supporting sections, are to be in accordance with the requirements of the Classification 
Society.  

I2.5.5   The effective span of a framing member is to be determined on the basis of its 
moulded length. If brackets are fitted, the effective span may be reduced in accordance with 
the usual practice of the Classification Society. Brackets are to be configured to ensure 
stability in the elastic and post-yield response regions. 

I2.5.6 When calculating the section modulus and shear area of a framing member, net 
thicknesses of the web, flange (if fitted) and attached shell plating are to be used. The shear 
area of a framing member may include that material contained over the full depth of the 
member, i.e. web area including portion of flange, if fitted, but excluding attached shell 
plating. 

FUnctions:
    frame_shearA_requirements - calculates the frame shear area requirement according to IACS
    frame_Z_requirements - calculates the frame section modulus requirement according to IACS
    frame_design - frame designer, initial seed design is such that satisfies the buckling requirements of IACS

�    Nc                 C   s�   | dkr@t ||�}|d }	d| | ||	 |  d|  }
|
}nj|d }	|| }|dk rj|dd|   }n|}dd|  }|| }d||	 |  d	 | | d|  }|}|S )
N�
transverse�   g     ��@gX9��v�?�   �      �?�333333�?i'  �      �?)�min)�framing�
span_frame�b_patch�PPF_calc�sigma_y�P_avg�AF�s_frame�LL�PPF�AtZAout�b_prime�b2�k0�b1�AL� r   �t/Users/mihkelkorgesaar/Documents/LRF report/Scantling calculations/polar_scantlings/Polar_Class_func/F02_framing2.py�frame_shearA_requirements-   s    
 $r   c           %   	   C   s  | dk�r<t ||�}|d }dd||   }d}t| |||||||�}|| }ddd|	 |   }d| }|||
 d  d ||d  d  d }|| }td�|�� |d	k r�d	}tdd|d  || d d|d  d d    d
d
d| |   dd|d    �}d| | | || |  | | d|  }|}n�|d }|| }|dk �rh|dd|   }n|}dd|  }|| }t| |||||||�} | | }!ddd|	 |   }"dd|"d|!d  d d    }#d|| |  | |d  |# d|  }$|$}|S )Nr   r   r   r   i�  �   ��  zkz={:5.2f} r   g      �?g       @g�������?g
ףp=
�?�ffffff�?i@B r   r   �   )r   r   �print�format�max)%r	   r
   r   r   r   r   r   r   �Aw�A_fn�t_s�b_f�t_f�t_pn�Zpr   r   �Y�jr   Za_1�kwZb_effZzpZkz�A1ZZptZZ0r   r   r   r   r   Za_4Zk_wlZA_4ZZpLr   r   r   �frame_Z_requirementsC   sD    

(."�,
 (r.   c
                  C   sd  t | |||||||�}
dt�|� d }t�|	�}d| |d d  }d}d}d}d}d	}d
}||
k sp||k �rPtd�|�� t�|| �}t�|| �}t�|| �}t�|| �}|| }t�d�}|| t�|� d }||
k r�|d7 }|d7 }q^|| d }|| ||  }|d }||d	  }|d	 }|| |k�r�|| d |d	 | t�|� d  ||t�|� |t�|�   d  }n�d| ||  d| |  d	|  }|| ||d	   t�|� || d	 |d	  | t�|� d ||| t�|� |t�|�   d   }t	| ||||||||||||||�}|d7 }|d7 }q^||||||||
fS )Ni%  g�������?gffffff�?��   r   r   g       @r   r   r   zFrame iteration={:3.0f} �Z   �d   �   i�  �
   r   )
r   �np�sqrt�ceilr    r!   �deg2rad�sin�cosr.   ) r	   r
   r   r   r   r   r   r   r%   �t_net�A0Zhwtwnr(   Zt_wnZtftwZbftwr#   r)   �Zreq�it�h_w�t_wr&   r'   Zh_stiffZfii_wr$   ZA_frameZApnZh_fcZb_wZz_nar   r   r   �frame_designl   sR    

L$l   �
r@   )�__doc__�numpyr4   r   r.   r@   r   r   r   r   �<module>   s   *)